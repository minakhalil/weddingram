<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateSetupTable extends Migration
{
    /**
     * Here we start creating our main tables
     *
     * @return void
     */
    public function up()
    {

      Schema::create('packages', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->unique();
          $table->string('icon');
          $table->string('color');
          $table->string('tip');

          $table->timestamps();
          $table->softDeletes();
      });



      Schema::create('categories', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->unique();
          $table->string('icon');
          $table->string('color');


          $table->timestamps();
          $table->softDeletes();
      });



        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('show_name');
            $table->string('avatar')->nullable();
            $table->string('url')->nullable();
            $table->string('phone');
            $table->boolean('active')->default(true);
            $table->boolean('verfied')->default(false);
            $table->integer('ads')->default(0);
            $table->text('bio')->nullable();
            $table->integer('type')->default(1);


            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');

            $table->integer('package_id')->unsigned();
            $table->foreign('package_id')->references('id')->on('packages');


            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->boolean('slidebar')->default(false);
            $table->boolean('video')->default(false);
            $table->integer('pirority')->default(0);


            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('Messages',function (Blueprint $table) {

            $table->text('message');
            $table->increments('id');

            $table->timestamps();
          $table->softDeletes();

        });
    }

    /**
     * Drop down tables
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
        Schema::dropIfExists('users');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('packages');
        Schema::dropIfExists('Messages');


    }
}
