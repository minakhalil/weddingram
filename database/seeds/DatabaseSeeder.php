<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Media;
use App\Models\Package;
use App\Models\Messages;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->delete();
        DB::table('packages')->delete();
        DB::table('categories')->delete();
        DB::table('media')->delete();

        Category::create(['name' => 'Photography','icon' => 'http://webneel.com/daily/sites/default/files/images/daily/05-2013/3-night-photography.preview.jpg','color' => 'blue']);
        Category::create(['name' => 'Videography','icon' => 'http://gratefulgeneration.com/wp-content/uploads/2014/04/videography.jpg','color' => 'violet']);
        Category::create(['name' => 'Makeup','icon' => 'http://images.wisegeek.com/makeup-being-applied-to-womans-face.jpg','color' => 'green']);
        Category::create(['name' => 'Dresses','icon' => 'https://www.allurebridals.com/assets/1550/collection-women.jpg','color' => 'green']);
        Category::create(['name' => 'Car rent','icon' => 'http://www.blueribbon.ie/wp-content/uploads/2014/06/12.jpg','color' => 'green']);
        Category::create(['name' => 'Wedding Planner','icon' => 'http://www.abcrnews.com/wp-content/uploads/2016/10/Surprising-Job-Responsibilities-of-Wedding-Planners.jpg','color' => 'green']);
        Category::create(['name' => 'Accessories','icon' => 'https://s-media-cache-ak0.pinimg.com/originals/f2/c6/8d/f2c68da0dfaa80d0a48832979873c3cb.jpg','color' => 'green']);
        Category::create(['name' => 'Furniture','icon' => 'https://storage.googleapis.com/united-with-love/uploads/2011/02/dramatic-furniture-for-wedding.jpg','color' => 'green']);


        Package::create(['name' => 'golden','icon' => 'photo','color' => 'yellow','tip' => 'hi golden']);
        Package::create(['name' => 'premium','icon' => 'photo','color' => 'black','tip' => 'hi premium']);
        Package::create(['name' => 'upcoming','icon' => 'photo','color' => 'red','tip' => 'hi upcoming']);


        User::create(['name' => 'main admin', 'email' => 'admin@admin.com', 'password' => bcrypt('123456789'), 'show_name' => 'dodo', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'01111111111','package_id'=>'1','category_id'=>'1','type'=>'10']);

        User::create(['name' => 'bisho', 'email' => 'bisho@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'bisho', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'1','category_id'=>'1','ads'=>'1']);
        User::create(['name' => 'mina', 'email' => 'mina@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'mina', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'2','category_id'=>'1']);
        User::create(['name' => 'bassem', 'email' => 'mina2@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'bassem', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'3','category_id'=>'1','ads'=>'1']);

        User::create(['name' => 'bisho kamal', 'email' => 'bisho2@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'bisho kamal', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'1','category_id'=>'1','ads'=>'1']);
        User::create(['name' => 'mina khalil', 'email' => 'bish3@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'mina khalil', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'2','category_id'=>'1']);
        User::create(['name' => 'bassem samir', 'email' => 'bis@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'bassem samir', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'3','category_id'=>'1','ads'=>'1']);

        User::create(['name' => 'brenda', 'email' => 'bish4@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'brenda', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'1','category_id'=>'3']);
        User::create(['name' => 'brenda adel', 'email' => 'bis5@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'brenda adel', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'2','category_id'=>'3','ads'=>'1']);
        User::create(['name' => 'brenda 2', 'email' => 'bis6@admin.com', 'password' => bcrypt('1234'), 'show_name' => 'brenda 2', 'avatar' => 'https://s-media-cache-ak0.pinimg.com/originals/39/e9/b3/39e9b39628e745a39f900dc14ee4d9a7.jpg','url'=>'dhfksdjhfhuefjkdfh','phone'=>'02111111111','package_id'=>'3','category_id'=>'3','ads'=>'1']);



        Media::create(['url' => 'https://s-media-cache-ak0.pinimg.com/736x/4e/96/2f/4e962f57aa3ddaaa9fdb617840f261fe.jpg','pirority' => '0','user_id' => '1']);
        Media::create(['url' => 'media1','pirority' => '2','user_id' => '3']);
        Media::create(['url' => 'hhd','pirority' => '5','user_id' => '2']);
        Media::create(['url' => 'aa','pirority' => '4','user_id' => '2']);
        Media::create(['url' => 'ee','pirority' => '1','user_id' => '5']);

        Messages::create(['message'=>'blablbla','id'=>'1']);
        Messages::create(['message'=>'ayhaaga','id'=>'2']);




    }
}
