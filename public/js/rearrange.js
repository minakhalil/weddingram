var Images = [];

$(document).ready(function(){
//////////////////////////////////////

    //Reference functions to reOrder HTML items
    //$("#div2").prependTo("#div3");
    //$("#div3").insertAfter("#div2");

    // Proof that the JS file is alive
    console.log("hey!");

    //testing the file ability to access the images class
    //$(".Image").css('visibility','hidden');

    // get the images table rows into an array 'Images'
    $('.Image').each(function(){
      let currentElement = $(this);
      //test
      //console.log(currentElement.attr('id'));
      Images.push(currentElement);

      if (currentElement.attr('id') == 1)
        currentElement.insertAfter('#firstImageTag');
    });


    //revese the array -> 1, 2, 3, 4, ....
    Images.reverse();

    // for each one in the array (except for item 1) place after
    // the preceding item (index - 1) which is the id , as the id
    // is the item's priority (see HTML file @blade)
    Images.forEach(function(item, index){
      if(item.attr('id')!=1){
        let number = parseFloat(item.attr('id')) - 1;
        //console.log(String(number));
        item.insertAfter("#" + String(number));
      }
      // console.log(item.attr('id'));
    });


});
