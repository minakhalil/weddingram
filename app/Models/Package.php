<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
class Package extends Model
{
    //
    public $table = "packages";
    use SoftDeletes;
    public static $GOLDEN_PACKAGE = "golden";
    public static $UPCOMING_PACKAGE = "upcoming";
    public static $PREMIUM_PACKAGE = "premium";

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'icon', 'color','tip'
    ];

    /*
    *
    * Define Relationships
    */

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
