<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
class Messages extends Model
{

    public $table = "Messages";
    use SoftDeletes;
     protected $fillable = ['message'];
}
