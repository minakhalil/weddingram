<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use App\Models\Category;
use App\Models\Package;
use App\Models\Media;
use App\Models\Messages;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Authenticatable
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public static $ADMIN = 10;
    public static $REGULAR_USER = 1;
    /**
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','show_name','avatar','url','active','ads','bio','type','category_id','package_id','phone','verfied'
    ];

    public function isAdmin()
    {
        if ((int)$this->type == self::$ADMIN) {
            return true;
        }

        return false;
    }

    /**
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /*
    *
    * Define Relationships
    */

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function media()
    {
        return $this->hasMany(Media::class);
    }
}
