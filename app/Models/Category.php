<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
    //
    public $table = "categories";
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'icon', 'color'
    ];

    /*
    *
    * Define Relationships
    */

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
