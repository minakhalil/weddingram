<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
class Media extends Model
{
    //
    public $table = "media";
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'url', 'slidebar', 'video','pirority','user_id'
    ];


    /*
    *
    * Define Relationships
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
