<?php

namespace App\Http\Middleware;

use Closure;
use \Auth;

class UserOrAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      /*  if (! $request->user()->isAdmin()) {
            return redirect()->action('UserController@youCantBeHere');
        }*/
          error_log(Auth::user());
        if (! Auth::user()->isAdmin()) {

            return redirect()->action('UserController@youCantBeHere');
        }
       return $next($request);
    }
}
