<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \Auth;
use DB;
use \stdClass;
use Session;
use Log;
Class UserController extends Controller
{
  public function __construct()
  {

  }
  public function changePassword (Request $request)
  {
    //brenda's
    $user=new stdClass();
    $newpass=new stdClass();
    $user->id =Auth::user()->id;
    $newpass=$request->input('newpass');

    $cryptedpass=Hash::make($newpass);

    DB::table('users')->where('id',$user->id)->update(['password'=> $cryptedpass]);

    if (Auth::user()->isAdmin())
      return redirect('/');

    //to make the user view and return it
    return redirect('vuser');

  }
  public function changeMail (Request $request)
  {
    //brenda's
    $user=new stdClass();
    $newmail= new stdClass();
    //$user->id = $request->input('userid');
    $user->id=Auth::user()->id;
    $newmail=$request->input('newmail');
    DB::table('users')->where('id',$user->id)->update(['email'=>  $newmail]);

    if (Auth::user()->isAdmin())
      return redirect('/');

    //to make the user view and return it
    return redirect('vuser');
  }

  public function changeUrl (Request $request)
  {
    $user=new stdClass();
    $user->id = Auth::user()->id;
    $newurl=$request->input('data');
    DB::table('users')->where('id',$user->id)->update(['url'=>  $newurl]);

    //to make the user view and return it
    return redirect('vuser');
  }

  public function changeBio (Request $request)
  {
    $user=new stdClass();
    $user->id = Auth::user()->id;
    $newbio=$request->input('data');
    DB::table('users')->where('id',$user->id)->update(['bio'=>  $newbio]);

    //to make the user view and return it
    return redirect('vuser');
  }

  public function changePhone (Request $request)
  {
    $user=new stdClass();
    $user->id = Auth::user()->id;
    $newPhone=$request->input('data');
    DB::table('users')->where('id',$user->id)->update(['phone'=>  $newPhone]);

    //to make the user view and return it
    return redirect('vuser');
  }

  public function changeName (Request $request)
  {
    $user=new stdClass();
    $user->id = Auth::user()->id;
    $newName=$request->input('data');
    DB::table('users')->where('id',$user->id)->update(['show_name'=>  $newName]);

    //to make the user view and return it
    return redirect('vuser');
  }

  public function logMeOut ()
  {
    Auth::logout();
    Session::flush();
    return redirect('/');
  }
  public function youCantAccessYet()
  {
    echo' waiting for verification';
  }
  public function youCantBeHere()
{
  echo 'you should not be here ';
}

}
