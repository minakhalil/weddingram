<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Package;
use App\Models\Media;

class ApiService extends Controller
{

  public function __construct(){
     // constructor ahooo ysta
  }

  public function allCategories(Request $request){
    $categories = Category::orderBy('id', 'asc')->get();
    return $categories;
  }

  public function allPackages(Request $request,Category $category){
    $packages = Package::all();
    $arr = array();
    foreach ($packages as $package) {
      $package->images = User::where('category_id',$category->id)->where('package_id',$package->id)->orderBy('id', 'desc')->limit(5)->select('avatar')->get();
      array_push($arr,$package);
    }
    return $arr;
  }

  public function listInPackage(Request $request,Category $category,Package $package){
    $users = User::where('category_id',$category->id)->where('package_id',$package->id)->get();


    return array(
      'package'=>$package,
      'users'=>$users
    );
  }

  public function homepage(Request $request){
    $admin_id=User::where('type', User::$ADMIN)->value('id');
    $announc=Media::where('user_id',$admin_id)->get();
    $ads=User::where('ads','<>','0')->orderBy('ads')->get();
    $res= array('announcment' =>$announc ,'ads'=>$ads );
    return $res;
  }

  public function goldenProfile(Request $request,User $user){
    $id=$user->id;
    $package_name=User::find($id)->package->name  ;
    //$package_name=
    if($package_name!=package::$GOLDEN_PACKAGE)
      return array('error' =>'this type of user does not have profile' );

    $user_profile=User::find($id);
    $user_profile->media=Media::where('user_id',$id)->get();
    return $user_profile;
  }
}
