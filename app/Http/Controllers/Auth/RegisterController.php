<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Input;
use \stdClass;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/verification';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


     // hena ana ba validate el data el dkhlali
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'show_name'=>'required|max:255',
            'phone'=>'required|min:11|max:11',
            //'category_id'=>'required|min:1|max:3',
           //  'package_id'=>'required|min:1|max:3',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {    $thepackage=new stdClass();
         $thecategory=new stdClass();
         $resultpackage = new stdClass();
          $resultcategory = new stdClass();
          //$resultcategory=1;
         $thepackage = Input::get('package');
         if ($thepackage == 'Premuim')
           $resultpackage=1;
         elseif ($thepackage=='Golden')
           $resultpackage=2;
         else
          $resultpackage=3; 

         $thecategory= Input::get('category');
          if ($thecategory == 'Photographers')
            $resultcategory=1;
          elseif ($thecategory=='HairDresser')
            $resultcategory=2;
          else
           $resultcategory=3;



         return User::create([
             'name' => $data['name'],
             'email' => $data['email'],
             'show_name'=>$data['show_name'],
             'phone'=>$data['phone'],
             'package_id'=>$resultpackage,
             'category_id'=>$resultcategory,

             'password' => bcrypt($data['password']),
         ]);
        //return $data['mmm'];
        // hwa hena bystkhdem model el user w ynadi fi function create el bt fill f table el User.
    }
}
