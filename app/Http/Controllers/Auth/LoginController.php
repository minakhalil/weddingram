<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/welcomepage';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    protected function authenticated (Request $request , $user)
    {
     // error_log($user);
 
      if ($user->isAdmin())
      { return redirect()->action('HomeController@index');}
      else {

        $verification= $user->verfied ;
        $notban=$user->active;
      error_log($notban);
      
        if ($verification==1)
        {
         if ($notban==1)
         return redirect()->action('HomeController@verifieduser');
else 
return redirect()->action('HomeController@notverifieduser');
         }
       
        else
        return redirect()->action('HomeController@notverifieduser');
      }
    }


}
