<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('home');
        $array = [
           "foo" => Auth::user()->name,
        ];
        return view('verification')->with('array', $array);
    }

    public function verifieduser()
    {
        $id = Auth::user()->id;
        $user=DB::table('users')->where('id',$id)->get();
        $media=DB::table('media')->where('user_id',$id)->get();
        $media = json_decode($media);
        return view('profile')->with('user', $user[0])->with('media', $media);
    }
    public function notverifieduser()
    {

        echo 'waiting for verification';

    }


}
