<!DOCTYPE html>
<html lang="en">
<?php $ip = 'http://localhost:8000' ?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Profile dashboard</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('dashboardTemplate/html/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{ asset('dashboardTemplate/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{ asset('dashboardTemplate/html/css/animate.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('dashboardTemplate/html/css/style.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('dashboardTemplate/html/css/colors/default.css') }}" id="theme" rel="stylesheet">

    <link href="{{ asset('css/profile_type1.css') }}" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>


@if ($user->package_id == '2')
    <link href="{{ asset('css/profile_type2-3.css') }}" rel="stylesheet">
@elseif ($user->package_id == '3')
    <link href="{{ asset('css/profile_type2-3.css') }}" rel="stylesheet">
@endif

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">

                <!-- /Logo -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <!-- search bar , i think it's not needed here<li>
                        <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li>-->
                    <li>
                        <a class="profile-pic" href="{{$ip}}/logout"> <img src="{{ asset('uploads/' . $user->avatar) }}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">Logout</b></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->

        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper"> <!-- edit da -->
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Profile page</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Profile Page</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-4 col-xs-12" id="profileBox">   <!-- here -->
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="{{ asset('uploads/' . $user->avatar) }}">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"><img src="{{ asset('uploads/' . $user->avatar) }}" class="thumb-lg img-circle" alt="img"></a>
                                        <h4 id="div3" class="text-white">{{$user->name}}</h4>
                                        <h5 id="div2" class="text-white">{{$user->email}}</h5> </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-12 col-sm-12  text-left">
                                    <p class="text-purple"><i class="ti-facebook"></i></p>
                                        <form class="form-horizontal form-material" method="POST">
                                            <div class="form-group">
                                                <label class="col-md-12 col-sm-12">Show Name</label>
                                                <div class="col-md-8 col-sm-8">
                                                    <input type="text" name="data" placeholder="{{$user->show_name}}" class="form-control form-control-line">
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /> </div>
                                                <div class="col-sm-4 col-md-6" >
                                                    <!-- TODO : 7ot hna new url that updates the database with the sent data, and redirect to the same page again (profile)-->
                                                    <button class="btn btn-success" formaction="{{$ip}}/changename">Update Show-Name</button>
                                                </div>
                                            </div>
                                        </form>
                                </div>
                                <div class="col-md-12 col-sm-12  text-left">
                                    <p class="text-purple"><i class="ti-facebook"></i></p>
                                        <form class="form-horizontal form-material" method="POST">
                                            <div class="form-group">
                                                <label class="col-md-12 col-sm-12">Phone</label>
                                                <div class="col-md-8 col-sm-8">
                                                    <input type="text" name="data" placeholder="{{$user->phone}}" class="form-control form-control-line">
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /> </div>
                                                <div class="col-sm-4 col-md-6" >
                                                    <!-- TODO : 7ot hna new url that updates the database with the sent data, and redirect to the same page again (profile)-->
                                                    <button class="btn btn-success" formaction="{{$ip}}/changephone">Update Phone</button>
                                                </div>
                                            </div>
                                        </form>
                                </div>

                                @if ($user->package_id == '1' || $user->package_id == '2')
                                <div class="col-md-12 col-sm-12  text-left">
                                    <p class="text-purple"><i class="ti-facebook"></i></p>
                                        <form class="form-horizontal form-material" method="POST">
                                            <div class="form-group">
                                                <label class="col-md-12 col-sm-12">Biography</label>
                                                <div class="col-md-8 col-sm-8">
                                                    <input type="text" name="data" placeholder="{{$user->bio}}" class="form-control form-control-line">
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /></div>
                                                <div class="col-sm-4 col-md-6" >
                                                    <!-- TODO : 7ot hna new url that updates the database with the sent data, and redirect to the same page again (profile)-->
                                                    <button class="btn btn-success" formaction="{{$ip}}/changebio">Update Biography</button>
                                                </div>
                                            </div>
                                        </form>
                                </div>
                                @endif
                                @if ($user->package_id == '1' || $user->package_id == '2')
                                <div class="col-md-12 col-sm-12  text-left">
                                    <p class="text-purple"><i class="ti-facebook"></i></p>
                                        <form class="form-horizontal form-material" method="POST">
                                            <div class="form-group">
                                                <label class="col-md-12 col-sm-12">URL</label>
                                                <div class="col-md-8 col-sm-8">
                                                    <input type="text" name="data" placeholder="{{$user->url}}" class="form-control form-control-line">
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /></div>
                                                <div class="col-sm-4 col-md-6" >
                                                    <!-- TODO : 7ot hna new url that updates the database with the sent data, and redirect to the same page again (profile)-->
                                                    <button class="btn btn-success" formaction="{{$ip}}/changeurl">Update URL </button>
                                                </div>
                                            </div>
                                        </form>
                                </div>
                                @endif
                                <div class="col-md-12 col-sm-12  text-left">
                                    <p class="text-purple"><i class="ti-facebook"></i></p>
                                        <form class="form-horizontal form-material" method="POST">
                                            <div class="form-group">
                                                <label class="col-md-12 col-sm-12">Email</label>
                                                <div class="col-md-8 col-sm-8">
                                                    <input type="text" name="newmail" placeholder="{{$user->email}}" class="form-control form-control-line">
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /> </div>
                                                <div class="col-sm-4 col-md-6" >
                                                    <!-- TODO : 7ot hna new url that updates the database with the sent data, and redirect to the same page again (profile)-->
                                                    <button class="btn btn-success" formaction="{{$ip}}/ajax/changemail">Update Email</button>
                                                </div>
                                            </div>
                                        </form>
                                </div>
                                <div class="col-md-12 col-sm-12  text-left">
                                    <p class="text-purple"><i class="ti-facebook"></i></p>
                                        <form class="form-horizontal form-material"  method="POST">
                                            <div class="form-group">
                                                <label class="col-md-12 col-sm-12">Password</label>
                                                <div class="col-md-8 col-sm-8">
                                                    <input type="password" name="newpass" class="form-control form-control-line" >
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /> </div>
                                                <div class="col-sm-4 col-md-6" >
                                                    <!-- TODO : 7ot hna new url that updates the database with the sent data, and redirect to the same page again (profile)-->
                                                    <button class="btn btn-success" formaction="{{$ip}}/ajax/changepassword">Update Password</button>
                                                </div>
                                            </div>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <form class="form-horizontal form-material">
                                <div class="form-group">
                                    <label class="col-md-12">Full Name</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <input type="password" value="password" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Phone No</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="123 456 7890" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Message</label>
                                    <div class="col-md-12">
                                        <textarea rows="5" class="form-control form-control-line"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Select Country</label>
                                    <div class="col-sm-12">
                                        <select class="form-control form-control-line">
                                            <option>London</option>
                                            <option>India</option>
                                            <option>Usa</option>
                                            <option>Canada</option>
                                            <option>Thailand</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> -->

                    @if ($user->package_id == '1')
                    <div class="col-sm-12 col-md-8">
                        <div class="white-box">
                            <h3 class="box-title">Images</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>icon</th>
                                            <th>Name</th>
                                            <th>Visible in Grid</th>
                                            <th>Visible in Slider</th>
                                            <th>Re-arrange</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id='firstImageTag'></tr>
                                        @foreach ($media as $image)

                                            <tr class="Image" id="{{ $image->pirority }}">
                                                <td> <img src="{{ asset('uploads/' . $image->url) }}" alt="user-img" width="80" class="img-thumbnail"></td>
                                                <td id="csser">{{$image->url}}</td>
                                                <td><label><input id="" type="checkbox"></label></td>
                                                <td><label><input id="" type="checkbox"></label></td>
                                                <td> <br> <i class="fa fa-arrow-up"></i> <br> <i class="fa fa-arrow-down"></i></td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <!-- <footer class="footer text-center"> 2017 &copy; Ample Admin brought to you by wrappixel.com </footer> -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="{{ asset('dashboardTemplate/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('dashboardTemplate/html/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('dashboardTemplate/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{ asset('dashboardTemplate/html/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('dashboardTemplate/html/js/waves.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('dashboardTemplate/html/js/custom.min.js') }}"></script>
    <!-- rearrange Javascript -->
    @if ($user->package_id == '1')
    <script src="{{ asset('js/rearrange.js') }}"></script>
    @endif
</body>

</html>
