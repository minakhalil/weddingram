@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <!-- hena law 3iza azwed input form  -->






                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>




 <!-- brenda zawdt men hena  -->


                        <div class="form-group{{ $errors->has('show_name') ? ' has-error' : '' }}">
                            <label for="show_name" class="col-md-4 control-label">Show_Name</label>
                              <div class="col-md-6">
                         <input id="show_name" type="text" name="show_name" >
                         @if ($errors->has('show_name'))
                             <span class="help-block">
                                 <strong>{{ $errors->first('show_name') }}</strong>
                             </span>
                         @endif
                         </div>
                        </div>


                         <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                             <label for="phone" class="col-md-4 control-label">Phone</label>
                               <div class="col-md-6">
                           <input id="phone" type="text" name="phone" >
                           @if ($errors->has('phone'))
                               <span class="help-block">
                                   <strong>{{ $errors->first('phone') }}</strong>
                               </span>
                           @endif
                          </div>
                        </div>


                        <div class="form-group{{ $errors->has('bio') ? ' has-error' : '' }}">

                          <!-- mkan w esm el label   -->

                            <label for="bio" class="col-md-4 control-label">Biography</label>
                            <!-- lih 3laka bel mkan bt3 casier   -->

                              <div class="col-md-6">

                                <!-- eli by3mel casier el inpit  -->

                          <input id="bio" type="text" name="bio" >


                          <!--function checking erros  -->

                          @if ($errors->has('bio'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('bio') }}</strong>
                              </span>
                          @endif



                         </div>
                       </div>


                    <!--    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="col-md-4 control-label">category</label>
                              <div class="col-md-6">  -->
                             <!--  <div>
					  <input type="radio" name="category" value="Photographers" checked> Photographers<br>
					  <input type="radio" name="category" value="BridalShop"> BridalShop<br>
					  <input type="radio" name="category" value="VideoMan"> VideoMan
					</div>  -->
                            <!--   <input id="category_id" type="text" name="category_id" >
                         @if ($errors->has('category_id'))
                             <span class="help-block">
                                 <strong>{{ $errors->first('category_id') }}</strong>
                             </span>
                         @endif



                         </div>
                       </div> -->

                      <label for="category_id" class="col-md-4 control-label">category</label>
                        <div class="col-md-6">

                          <div>
                            <input type="radio" name="category" value="Photographer" checked>Photographer <br>
                            <input type="radio" name="category" value="HairDresser"> HairDresser<br>
                            <input type="radio" name="category" value="Videoman"> videoman
                          </div>


                          <!--  <input id="category_id" type="text" name="category_id" > -->

                         </div>





                    <!--   <div class="form-group{{ $errors->has('package_id') ? ' has-error' : '' }}"> -->
                            <label for="package_id" class="col-md-4 control-label">package</label>
                             <div class="col-md-6">
                      <!-- <input id="package_id" type="text" name="package_id" > -->

                				        <div>
                          					  <input type="radio" name="package" value="Premuim" checked> Premuim<br>
                          					  <input type="radio" name="package" value="Golden"> Golden<br>
                          					  <input type="radio" name="package" value="Upcoming"> Upcoming
                					     </div>

                             </div>
                        <!--</div> -->




                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
