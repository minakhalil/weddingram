<?php


use App\Models\User;
use App\Models\Messages ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


//FOR ALL USERS
Route::get('/verification','UserController@youCantAccessYet')->middleware('checkLogin');
Route::post('ajax/changemail', 'UserController@changeMail')->middleware('checkLogin');
Route::post('ajax/changepassword', 'UserController@changePassword')->middleware('checkLogin');
Route::post('/changeurl', 'UserController@changeUrl');
Route::post('/changebio', 'UserController@changeBio');
Route::post('/changephone', 'UserController@changePhone');
Route::post('/changename', 'UserController@changeName');
Route::get('/logout', 'UserController@logMeOut');
Route::get('/AccessingOutOfDomain', 'UserController@youCantBeHere');

Route::get('/settings',function(){
	return view('settings');
})->middleware('checkLogin');

//Route::get('/a', function(){return view('base');});
// this function implement logout ( clear session ) but i'm not sure if it's already implemented.
/*Route::get('/admin/search/{userName}', function (string $userName)
{
MINA A2L HAN3MELHA F FROMT MSH HENA

} );*/



//FOR LOGIN
Auth::routes();
/* Route::auth() is a method that cleanly encapsulates all the login and register routes.*/
Route::get('/', 'HomeController@index')->middleware('MyTypeOfUser');
Route::get('/vuser', 'HomeController@verifieduser');
Route::get('/nvuser', 'HomeController@notverifieduser');
Route::get('/admin/verify','AdminController@verifyUsers');

Route::get('/bassem/dummy', function(){
	// $user=DB::table('users')->where('id',4)->get();
	// $user = json_decode($user);
	// //print_r($user[0]); // da objject keda msh array
	//
	// foreach($user[0] as $name => $value) {
  //    echo $name . ": " . $value . '<br>';
	// 	}
	$media=DB::table('media')->where('user_id',4)->get();
	$media = json_decode($media);
	foreach ($media as $mediaItem) {
			foreach ($mediaItem as $key => $value) {
				echo $key . ": " . $value . '<br>';
			}
			echo "<br>";
	}
});


//FOR ADMIN
Route::post('ajax/admin/verifyMe', 'AdminController@verifyMe')->middleware('checkLogin','MyTypeOfUser');
Route::get('/admin/viewusers', 'AdminController@viewUsers')->middleware('checkLogin','MyTypeOfUser');
Route::get('/admin/viewusers/ban/{user}','AdminController@banUser')->middleware('checkLogin','MyTypeOfUser');
Route::get('/admin/viewmessages','AdminController@viewMessages')->middleware('checkLogin','MyTypeOfUser');
Route::get('/admin/viewdashboard', 'AdminController@viewDashboard')->middleware('checkLogin','MyTypeOfUser');
