<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middlewareGroups' => ['api'],'middleware' => ['cors']], function () {

  Route::get('/home', 'ApiService@homepage');
  Route::get('/user/golden/{user}','ApiService@goldenProfile');
  Route::get('/categories', 'ApiService@allCategories');
  Route::get('/{category}/packages', 'ApiService@allpackages');
  Route::get('/{category}/{package}', 'ApiService@listInPackage');

});
